package com.example.chui.message;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

/**
 * Created by ndhuy13 on 10/07/2017.
 */

public class Database extends SQLiteOpenHelper {
    public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void queryData(String sql){              // CREATE. UPDATE, INSERT, DROP
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }

    public Cursor getData(String sql){              // SELECT
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql,null);
    }


    /*
    Support function
     */

    public void init(){             // Create table
        String createTableUser = "CREATE TABLE IF NOT EXISTS User" +
                "(" +
                "ID VARCHAR(100) PRIMARY KEY NOT NULL, " +
                "UserName VARCHAR(100) NOT NULL, "+
                "Avatar BLOB " +
                ")" ;
        this.queryData(createTableUser);

        String createTableChatMessageText = "CREATE TABLE IF NOT EXISTS ChatMessages" +
                "(" +
                "ID1 VARCHAR(100) NOT NULL, " +
                "ID2 VARCHAR(100) NOT NULL, "+
                "TIME DATETIME NOT NULL, "+             //Format: YYYY-MM-dd hh:mm:ss
                "isID1 INTEGER NOT NULL, " +
                "Content TEXT, "+
                "NoOfImg INTEGER, "+
                "PRIMARY KEY(ID1, ID2, TIME), "+
                "FOREIGN KEY(ID1) REFERENCES User(ID), "+
                "FOREIGN KEY(ID2) REFERENCES User(ID) "+
                ")" ;
        this.queryData(createTableChatMessageText);

        String createTableIMAGE = "CREATE TABLE IF NOT EXISTS IMAGE" +
                "(" +
                "ID1 VARCHAR(100) NOT NULL, " +
                "ID2 VARCHAR(100) NOT NULL, "+
                "TIME DATETIME, "+
                "IMG BLOB, "+
                "PRIMARY KEY(ID1,ID2, TIME, IMG), "+
                "FOREIGN KEY(ID1) REFERENCES User(ID), "+
                "FOREIGN KEY(ID2) REFERENCES User(ID) "+
                ")" ;
        this.queryData(createTableIMAGE);
    }


    /*
    Return type: a list in Message type
     */
    public ArrayList<Messages> getMessages(){
        ArrayList<Messages> arr = new ArrayList<>();
        String loadMessages = "SELECT * " +
                "FROM ChatMessages";
        Cursor cursor = this.getData(loadMessages);
        int numOfImg;

        if (cursor.moveToFirst()){
            do{
                numOfImg = cursor.getInt(5);
                if (numOfImg == 0){     //Chat messages have only text
                    arr.add(new Messages(cursor.getString(0), cursor.getString(1), cursor.getInt(3),
                            cursor.getString(2),cursor.getString(4),null));
                }
                else if (numOfImg == 1){ //Not only text, but also img
                    byte[] img = this.getImage(cursor.getString(0),cursor.getString(1),cursor.getString(2));
                    arr.add(new Messages(cursor.getString(0), cursor.getString(1) , cursor.getInt(3),
                            cursor.getString(2), cursor.getString(4),img));
                }
                else{                   //Only Image
                    ArrayList<byte[]> arrImgs = this.getImages(cursor.getString(0),cursor.getString(1),cursor.getString(2));
                    arr.add(new Messages(cursor.getString(0), cursor.getString(1) , cursor.getInt(3),cursor.getString(2), arrImgs));
                }
            }while(cursor.moveToNext());
        }



        return arr;
    }

    public void insertUser(String ID, String userName, byte[] img){
        SQLiteDatabase database = getWritableDatabase();
        String insert = "INSERT INTO User VALUES(?,?,?)";
        SQLiteStatement statement = database.compileStatement(insert);
        statement.clearBindings();

        statement.bindString(1, ID);
        statement.bindString(2, userName);
        if(img == null){
            statement.bindNull(3);
        }
        else statement.bindBlob(3, img);

        statement.executeInsert();
    }

    public void insertChatMessages(String ID1, String ID2, String time, int isID1, String content, int numOfImg){
        String insert = "INSERT INTO ChatMessages Values('" +
                ID1 + "','"+
                ID2 + "','"+
                time + "','"+
                isID1 + "','"+
                content + "','"+
                numOfImg + "')";
        this.queryData(insert);
    }

    public void insertImage(String ID1, String ID2, String time, byte[] img){
        String insert = "INSERT INTO IMAGE Values(?,?,?,?)";
        SQLiteDatabase database = getWritableDatabase();
        SQLiteStatement statement = database.compileStatement(insert);
        statement.clearBindings();

        statement.bindString(1, ID1);
        statement.bindString(2, ID2);
        statement.bindString(3, time);
        statement.bindBlob(4, img);

        statement.executeInsert();
    }

    public byte[] getImage(String ID1, String ID2, String time){
        String sql = "SELECT IMG FROM IMAGE WHERE ID1='"+
                ID1 + "' AND ID2 ='" + ID2 +"' AND TIME ='"+ time +"'" ;
        Cursor cursor = this.getData(sql);
        cursor.moveToFirst();
        byte[] img = cursor.getBlob(0);
        return img;
    }

    public ArrayList<byte[]> getImages(String ID1, String ID2, String time){
        ArrayList<byte[]> arr = new ArrayList<>();
        String sql = "SELECT IMG FROM IMAGE WHERE ID1='"+
                ID1 + "' AND ID2 ='" + ID2 +"' AND TIME ='"+ time +"'" ;
        Cursor cursor = this.getData(sql);
        if (cursor.moveToFirst()){
            do{
                arr.add(cursor.getBlob(0));
            }while(cursor.moveToNext());
        }
        return arr;
    }

    public byte[] getAvatar(String ID){
        byte[] avatar;
        String sql = "SELECT Avatar FROM User WHERE ID='" + ID +"'";
        Cursor cursor = this.getData(sql);
        cursor.moveToFirst();
        avatar = cursor.getBlob(0);
        return avatar;
    }

    public String getUserName(String ID){
        String sql = "SELECT Avatar FROM User WHERE ID='" + ID +"'";
        Cursor cursor = this.getData(sql);
        cursor.moveToFirst();
        String userName = cursor.getString(0);
        return userName;
    }
}
