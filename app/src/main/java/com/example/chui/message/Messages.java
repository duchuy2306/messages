package com.example.chui.message;


import java.util.ArrayList;

/**
 * Created by ndhuy13 on 10/07/2017.
 */

public class Messages {
    private String ID1;                     // ID of One person
    private String ID2;                     // ID of another
    private int isID1;                      // 1 : ID1  else   0: ID2
    private String Time;                    // timestamp
    private String messages;                // content
    private byte[] img = null;
    private ArrayList<byte[]> imgLst;       // Use for send a list img - only img

    public Messages(String ID1, String ID2, int isID1, String time, String messages, byte[] img) {
        this.ID1 = ID1;
        this.ID2 = ID2;
        this.isID1 = isID1;
        Time = time;
        this.messages = messages;
        this.img = img;
        imgLst = null;
    }

    public Messages(String ID1, String ID2, int isID1, String time, ArrayList<byte[]> imgLst) {
        this.ID1 = ID1;
        this.ID2 = ID2;
        this.isID1 = isID1;
        Time = time;
        this.imgLst = imgLst;
        messages = null;
    }

    public String getID1() {
        return ID1;
    }

    public void setID1(String ID1) {
        this.ID1 = ID1;
    }

    public String getID2() {
        return ID2;
    }

    public void setID2(String ID2) {
        this.ID2 = ID2;
    }

    public int getIsID1() {
        return isID1;
    }

    public void setIsID1(int isID1) {
        this.isID1 = isID1;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public ArrayList<byte[]> getImgLst() {
        return imgLst;
    }

    public void setImgLst(ArrayList<byte[]> imgLst) {
        this.imgLst = imgLst;
    }
}

