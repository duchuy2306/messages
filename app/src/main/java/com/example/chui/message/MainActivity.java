package com.example.chui.message;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Database database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setHomeAsUpIndicator(R.drawable.icback);
        actionBar.setDisplayHomeAsUpEnabled(true);

        //Init Database
        database = new Database(this,"CsMessager", null, 1);
        database.init();

        //database.insertUser("ndhuy","Huy",null);
        //database.insertChatMessages("ndhuy","hdth","2017-07-11 14:53:00",0,"Hi",0);

        ArrayList<Messages> arr = database.getMessages();
        for(int i=0; i<arr.size(); i++){
            Toast.makeText(MainActivity.this, arr.get(i).getMessages(),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.newMenu:
                //TODO
                break;
            case R.id.videoCall:
                //TODO
                break;
            case R.id.voiceCall:
                //TODO
                break;
            case R.id.home:             // Back
                //TODO
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

